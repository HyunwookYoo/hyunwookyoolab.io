---
layout: single
title: About
permalink: /about/
author_profile: true
---

I was born in South Korea and live for about 20 years. Since when I was in high school, I was interested in game development and studied C++ and Unreal Engine 4  and 5. 

Currently I work at Epoch Games and in charge of making HUD and UI system. Personally I am interested in Unity game development. 

I play video games a lot as a hobby. I really like RPG such as The Witcher 3, Divinity Original Sin series, and Wartales.
